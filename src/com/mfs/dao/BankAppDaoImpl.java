package com.mfs.dao;

import javax.jws.WebService;
import javax.persistence.Query;

import org.hibernate.Session;

import com.mfs.constants.BankAppConstants;
import com.mfs.exceptions.BankAppCustomException;
import com.mfs.pojo.BankAppEntity;
import com.mfs.utility.HibernateUtil;

@WebService(endpointInterface = "com.mfs.dao.BankAppDao")
public class BankAppDaoImpl implements BankAppDao {

	private Session session;

	static{
		HibernateUtil.initializeSessionFactory();
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.mfs.dao.BankAppDao#withdrawAmount(java.lang.Integer, java.lang.Double)
	 */
	@Override
	public Double withdrawAmount(Integer accno, Double amount) throws BankAppCustomException {

		BankAppEntity accounts = findByAccno(accno);
		 Double balance = accounts.getBalance();
		if (balance <= 0) {
			throw new BankAppCustomException(
					Thread.currentThread().getName() + BankAppConstants.ZEROBAL + " balance is " + accounts.getBalance());
		} else if (balance <= 300) {
			throw new BankAppCustomException(Thread.currentThread().getName() + BankAppConstants.MINIMUMBAL);
		} else if (balance >= amount) {
			if (amount != 0 && amount % 100 == 0) {
				System.out.println(Thread.currentThread().getName() + " " + amount + " is going to withdraw");
				return withdraw(amount, accno ,accounts);
			} else {
				throw new BankAppCustomException(Thread.currentThread().getName() + BankAppConstants.NOTINMULTIPLEOF100);
			}
		} else {
			throw new BankAppCustomException(
					Thread.currentThread().getName() + BankAppConstants.INSUFFICINTBAL + " " + balance);
		}
	}

	/**
	 * method to perform withdraw operation
	 * @param amt amount to withdraw
	 * @param custid customer id
	 * @throws CustomException customer exception
	 */
	private Double withdraw(Double amt, int accno, BankAppEntity accounts) {

		Double balance = accounts.getBalance();
		balance = balance - amt;
		accounts.setBalance(balance);
		try{
		session.update(accounts);
		session.getTransaction().commit();
		return accounts.getBalance();
		}finally{
		session.close();
		}
	}
	
	/**
	 * method to deposite ammount
	 * @param amt amount to deposite
	 * @param accno customer accno
	 * @throws CustomException 
	 */
	@Override
	public Double depositewAmount(Integer accno, Double amount) throws BankAppCustomException {

		if(amount <= 0){
			throw new BankAppCustomException("Entered amount to deposite is incorrect!!!!");
		}
		BankAppEntity accounts =  findByAccno(accno);
		 Double balance = accounts.getBalance();
		balance = balance + amount;
		accounts.setBalance(balance);
		try{
		session.update(accounts);
		session.getTransaction().commit();
		return accounts.getBalance();
		}finally{
		session.close();
		}
	
	}

	/**
	 * method to get Bankdetails
	 * id isustomer id 
	 */
	@Override
	public BankAppEntity getBankDetails(Integer id) throws BankAppCustomException {
		BankAppEntity account = null;
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try{
		account = (BankAppEntity)session.load(BankAppEntity.class, id);
		System.out.println(account.getUsername());
		System.out.println(account.getAccno());
		System.out.println(account.getAcctype());
		System.out.println(account.getBalance());
		return account;
		}catch(Exception e){
			throw new BankAppCustomException("resource not found",new Throwable("404"));
		}
		finally{
			session.close();
		}
	}
	
	/**
	 * method to find accounts by account number
	 * @param accno account number
	 * @return accounts
	 */
	private BankAppEntity findById(Integer userid){
		BankAppEntity accounts = null;
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try{
		accounts = (BankAppEntity)session.load(BankAppEntity.class, userid);
		return accounts;
		}finally{
			
		}
		
	}

	/**
	 * method to add user bank details into table
	 */
	@Override
	public void addUser(String username, String password, Integer accno, String acctype, Double balance)throws BankAppCustomException {

		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		BankAppEntity account = new BankAppEntity();
		account.setCustid(19);
		account.setUsername(username);
		account.setPassword(password);
		account.setAccno(accno);
		account.setAcctype(acctype);
		account.setBalance(balance);
		try{
		session.save(account);
		session.getTransaction().commit();
		}finally{
			session.close();
		}
		
	}
	
	/**
	 * method to find accounts by account number
	 * @param accno account number
	 * @return
	 */
	public BankAppEntity findByAccno(int accno) {
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		String hql = "FROM BankAppEntity A WHERE A.accno =" + accno;
		Query query = session.createQuery(hql);
		BankAppEntity accounts = (BankAppEntity) query.getSingleResult();
		return accounts;
	}

}
