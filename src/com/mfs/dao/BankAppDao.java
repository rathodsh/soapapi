package com.mfs.dao;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.mfs.exceptions.BankAppCustomException;
import com.mfs.pojo.BankAppEntity;

@WebService
public interface BankAppDao {

	@WebMethod public Double withdrawAmount(Integer accno, Double amount) throws BankAppCustomException;
	@WebMethod public Double depositewAmount(Integer accno, Double amount) throws BankAppCustomException;
	@WebMethod public BankAppEntity getBankDetails(Integer accno) throws BankAppCustomException;
	@WebMethod public void addUser(String username,String password,Integer accno,String acctype,Double balance) throws BankAppCustomException;
}
