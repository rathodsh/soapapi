package com.mfs.constants;

public class BankAppConstants {
	
	public static final String INSUFFICINTBAL = "insufficient balance" ;
	public static final String ZEROBAL = "account is overdrawn!";
	public static final String NOTINMULTIPLEOF100 = "please enter amount in multiple of hundreds";
	public static final String MINIMUMBAL = "you do not withdraw money.!!! please maintain minimum balance of 300";
	public static final String PERDAYLIMIT = "you can not withdraw more than 25000 per day";

}
