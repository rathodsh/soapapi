package com.mfs.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name ="bankdetails")
public class BankAppEntity {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column
	private Integer custid;
	@Column
	private String username;
	@ColumnTransformer( read="AES_decrypt(password ,'00')" ,write="AES_encrypt(? , '00')")
	private String password;
	@Column
	private Integer accno;
	@Column
	private String acctype;
	@Column
	private Double balance;
	
	public BankAppEntity() {
	}
	
	public BankAppEntity(Integer custid, String username, String password, Integer accno, String acctype,
			Double balance) {
		super();
		this.custid = custid;
		this.username = username;
		this.password = password;
		this.accno = accno;
		this.acctype = acctype;
		this.balance = balance;
	}

	public Integer getCustid() {
		return custid;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public Integer getAccno() {
		return accno;
	}

	public String getAcctype() {
		return acctype;
	}

	public Double getBalance() {
		return balance;
	}

	public void setCustid(Integer custid) {
		this.custid = custid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAccno(Integer accno) {
		this.accno = accno;
	}

	public void setAcctype(String acctype) {
		this.acctype = acctype;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "BankAppEntity [custid=" + custid + ", username=" + username + ", password=" + password + ", accno="
				+ accno + ", acctype=" + acctype + ", balance=" + balance + "]";
	}
	
	

}
