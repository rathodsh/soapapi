package com.mfs.service;

import com.mfs.pojo.BankAppEntity;

public interface BankAppService {
	
	public void withdrawAmount(Integer accno, Double amount);
	public void depositeAmount(Integer accno, Double amount);
	public BankAppEntity getBankDetails(Integer accno);
	public void addUser(String username,String password,Integer accno,String acctype,Double balance);

}
