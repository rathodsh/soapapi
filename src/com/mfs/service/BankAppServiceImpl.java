package com.mfs.service;

import com.mfs.dao.BankAppDaoImpl;
import com.mfs.exceptions.BankAppCustomException;
import com.mfs.pojo.BankAppEntity;

public class BankAppServiceImpl implements BankAppService {

	@Override
	public void withdrawAmount(Integer accno, Double amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void depositeAmount(Integer accno, Double amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public BankAppEntity getBankDetails(Integer accno) {
		return null;//new BankAppDaoImpl().getBankDetails(accno);
	}

	@Override
	public void addUser(String username, String password, Integer accno, String acctype, Double balance) {
		try {
			new BankAppDaoImpl().addUser(username, password, accno, acctype, balance);
		} catch (BankAppCustomException e) {
			System.out.println(e.getMessage());
		}
	}

}
