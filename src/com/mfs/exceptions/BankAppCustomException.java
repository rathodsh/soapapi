package com.mfs.exceptions;

public class BankAppCustomException extends Exception {

	public BankAppCustomException(String message){
		super(message);
	}
	
	public BankAppCustomException(String message,Throwable cause){
		super(message,cause);
	}
}
